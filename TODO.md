Bugs
----

- If image is not found is not detected as a download error

Technical debt
----

- verify and install gsettings if needed
- verify and install curl if needed