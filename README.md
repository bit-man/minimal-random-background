minimal random background
===========

Pluggable desktop wallpaper changer

History
-------

Started as a single morning bash project and sooner than later
realized that will surpass this goal that converted it to a 
pluggable artifact. Still can be used only at Linux distros 
running Cinnamon Desktop Environment, but not hard to change
to support a different one 

Configuration
-----

Create an executable bash file at `$HOME/.random-background/settings` 
that will be used to configure your wallpaper changer :

* strategy : wallpaper change strategy to use (only `bit-man-selections` is supported)
* waitTimeInSecs : seconds that each wallpaper will be show

```
#!/usr/bin/env bash

strategy=bit-man-selections
waitTimeInSecs=300
```

Usage
---

Run the file `random-background`. It will take a while to 
download all images before starting to show your selected
wallpapers

Create your strategies
---

Strategies are different ways to retrieve images to be used
as background images. To create your own
* Add a new bash script to strategies folder
* Make it executable
* Create the `loadImages` function that will setup the folder 
where background images are (or will be) stored, returning
the folder path

```
function loadImages() {
     ## Do you stuff, copy files, download, and so on

    echo $imagePath
}
```

* At setting file change the `strategy` setting to point to your newly created strategy

Credits
-------

Icon extracted from https://wallpaperaccess.com/random-minimalist#816861